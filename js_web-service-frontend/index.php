<?php
session_start();

if(empty($_SESSION['authenticated']) || $_SESSION['authenticated'] !== "yes"){
	header("Location: login.php");
	exit();
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Welcome to my final project</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script src="js/ajax.js"></script>
	<script src="js/test-module.js"></script>
	<script src="js/RequestModule.js"></script>
	<script src="js/UserFormModule.js"></script>
	<script src="js/ResponseModule.js"></script>
	<script src="js/main.js"></script>
</head>
<body>
	<nav class="navbar navbar-dark bg-primary">
		<h2>Simple Web Service</h2>
	</nav>
	<div class="container-fluid p-5">
		<h3>This RESTful web service allows an authenticated user to access a database and perform CRUD operations using AJAX calls.
			It is built with modular JavaScript that can be plugged in to any database or server-side code. 
		</h3>
		<h4 class="mt-3">This example uses a database that stores information about restaurants.</h4>
	</div>
	<div class="container-fluid pl-5 pb-3">
		<h4>Select an AJAX call below to utilize the service.</h4>
	</div>
	<div class="container-fluid">
		<div class="row m-3">
			<div id="request-container" class="col-lg-4"></div>
			<div id="form-container" class="col-lg-3 mb-5 ml-sm-5 ml-lg-0"></div>
			<div id="response-container" class="col-lg-5"></div>
		</div>
	</div>
	
</body>
</html>
