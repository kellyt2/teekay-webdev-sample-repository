var teekaywebdev = teekaywebdev || {};

teekaywebdev.UserFormModule = function(options){

	var container = options.formContainer || null; 

    var restaurant = {restaurantName: "", restaurantAddress: "", restaurantCity: "", 
    restaurantState: "", restaurantZip: "", restaurantPhone:""};

    var form = `<div class="user-form">`;	

    for (property in restaurant) {
        var key = property;
        //var value = restaurant[property];
        form +=
            `<div>
				<label>${property}</label>
				<div class="validation v${property}"></div>
				<input type="text" name="${property}" class="txt${property}">	
            </div>`;
    }	
    form +=	
        `<div>
            <label></label>
            <input type="button" class="btnCancel" value="CLEAR">
        </div>`;			

    // UI instance variables
    var txtId = null;
    var txtRestName = null;
    var txtRestAddress = null;
    var txtRestCity = null;
    var txtRestState = null;
    var txtRestZip = null;
    var txtRestPhone = null;
    var btnCancel = null;

    var vRestName= null;
    var vRestAddress = null;
    var vRestCity = null;
    var vRestState = null;
    var vRestZip = null;
    var vRestPhone = null;

    var regexPhone = null;

    initialize();

	///////////////////////////////
	// METHODS
	///////////////////////////////

	function initialize(){
        container.innerHTML = form;
        txtId = container.querySelector(".txtId");
        txtRestName = container.querySelector(".txtrestaurantName");
        txtRestAddress = container.querySelector(".txtrestaurantAddress");
        txtRestCity = container.querySelector(".txtrestaurantCity");
        txtRestState = container.querySelector(".txtrestaurantState");
        txtRestZip = container.querySelector(".txtrestaurantZip");
        txtRestPhone = container.querySelector(".txtrestaurantPhone");

        vRestName = container.querySelector(".vrestaurantName");
        vRestAddress = container.querySelector(".vrestaurantAddress");
        vRestCity = container.querySelector(".vrestaurantCity");
        vRestState = container.querySelector(".vrestaurantState");
        vRestZip = container.querySelector(".vrestaurantZip");
        vRestPhone = container.querySelector(".vrestaurantPhone");

        btnSave = container.querySelector(".btnSave");
        btnCancel = container.querySelector(".btnCancel");

        regexPhone = /^[0-9-()\s]{7,14}$/;

		btnCancel.addEventListener("click", clearForm);
    }
    
    function clearForm(){
		txtRestName.value = "";
        txtRestAddress.value = "";
        txtRestCity.value = "";
        txtRestState.value = "";
        txtRestZip.value = "";
		txtRestPhone.value = "";
		
		clearErrorMessages();	
    }
    
    function validate(){
		// This method should return true if the user input is valid, false otherwise
		// It should also display the appropriate validation error messages in the user interface
		clearErrorMessages();
		var isValid = true;
		var firstInvalidField = null;
		if(!validateRestName()){
			firstInvalidField = txtRestName;
			isValid = false;
		}
		if(!validateRestAddress()){
			if(firstInvalidField == null){
				firstInvalidField = txtRestAddress;
			}
            isValid = false;
            
		}
		if(!validateRestCity()){
			if(firstInvalidField == null){
				firstInvalidField = txtRestCity;
			}
			isValid = false;
		}
		if(!validateRestState()){
			if(firstInvalidField == null){
				firstInvalidField = txtRestState;
			}
			isValid = false;
        }
        if(!validateRestZip()){
			if(firstInvalidField == null){
				firstInvalidField = txtRestZip;
			}
			isValid = false;
        }
        if(!validateRestPhone()){
			if(firstInvalidField == null){
				firstInvalidField = txtRestPhone;
			}
			isValid = false;
		}

		if(firstInvalidField != null){
			firstInvalidField.focus();	
		}
		return isValid;
    }
    
    function validateRestName(){
		var isValid = true;
		if(txtRestName.value == ""){
				isValid = false;
				vRestName.textContent = "You must enter a restaurant name";
				vRestName.style.visibility = "visible";
		}
		return isValid;
    }

    function validateRestAddress(){
		var isValid = true;
		if(txtRestAddress.value == ""){
				isValid = false;
				vRestAddress.textContent = "You must enter a restaurant address";
				vRestAddress.style.visibility = "visible";
		}
		return isValid;
    }

    function validateRestCity(){
		var isValid = true;
		if(txtRestCity.value == ""){
				isValid = false;
				vRestCity.textContent = "You must enter a restaurant city";
				vRestCity.style.visibility = "visible";
		}
		return isValid;
    }

    function validateRestState(){
		var isValid = true;
		if(txtRestState.value == ""){
				isValid = false;
				vRestState.textContent = "You must enter a restaurant state";
				vRestState.style.visibility = "visible";
		}
		return isValid;
    }

    function validateRestZip(){
		var isValid = true;
		if(txtRestZip.value == ""){
				isValid = false;
				vRestZip.textContent = "You must enter a restaurant zip";
				vRestZip.style.visibility = "visible";
		}
		return isValid;
    }

    function validateRestPhone(){
		var isValid = true;
		if(txtRestPhone.value == ""){
				isValid = false;
				vRestPhone.textContent = "You must enter a restaurant phone";
				vRestPhone.style.visibility = "visible";
		}else if(!(regexPhone.test(txtRestPhone.value))){
				isValid = false;
				vRestPhone.textContent = "You entered an invalid phone number. Valid format: \"xxx-xxx-xxxx\"";
				vRestPhone.style.visibility = "visible";
		}else{
				vRestPhone.style.visibility = "hidden";
		}
		return isValid;
    }

	function clearErrorMessages(){

		vRestName.textContent = "";
		vRestName.style.visibility = "hidden";

		vRestAddress.textContent = "";
		vRestAddress.style.visibility = "hidden";

		vRestCity.textContent = "";
		vRestCity.style.visibility = "hidden";

		vRestState.textContent = "";
		vRestState.style.visibility = "hidden";

		vRestZip.textContent = "";
		vRestZip.style.visibility = "hidden";

		vRestPhone.textContent = "";
		vRestPhone.style.visibility = "hidden";
	}

	function submit(id){
		
		if(validate()){
			
			var obj = {
				restaurantId: id || null,
				restaurantName: txtRestName.value,
				restaurantAddress: txtRestAddress.value,
				restaurantCity: txtRestCity.value,
				restaurantState: txtRestState.value,
				restaurantZip: txtRestZip.value,
				restaurantPhone: txtRestPhone.value
            };
			var json = JSON.stringify(obj);
			clearForm();
			return json;
		}else{
			return null;
		}
	}

	return{
		submit: submit
	}

}


