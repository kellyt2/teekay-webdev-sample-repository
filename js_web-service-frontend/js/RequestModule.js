var teekaywebdev = teekaywebdev || {};


/**
* Creates an instance of the Request Module: renders UI on the page and enables AJAX calls from UI.
* @param {object} 		options	
* @param {HTML Element} options.listContainer 	The HTML element which will contain the Request UI
* @param {HTML Element} options.detailsContainer 	The HTML element which will contain the Response data and UI
* @param {string}	    options.webServiceURL	This URL of the web service to receive AJAX calls
*/
teekaywebdev.RequestModule = function(options) {
	
	//////////////////////////////////////
	// INSTANCE VARIABLES
	//////////////////////////////////////

	var listContainer = options.listContainer || null;
	var detailsContainer = options.detailsContainer || null;
	var baseURL = options.webServiceURL || "//localhost/adv-topics/web-services/restaurants/";

	var userFormModule = new teekaywebdev.UserFormModule({
		formContainer: document.getElementById("form-container")
	});


	var template = 
		`<div class="list-group pl-5 pr-5 pb-5">
			<button type="button" class="list-group-item list-group-item-action" id="btnGetAll">GET ALL</button>
			<button type="button" class="list-group-item list-group-item-action" id="btnGetById">GET BY ID</button>
			<button type="button" class="list-group-item list-group-item-action" id="btnPost">POST</button>
			<button type="button" class="list-group-item list-group-item-action" id="btnPut">PUT/UPDATE</button>
			<button type="button" class="list-group-item list-group-item-action" id="btnDelete">DELETE</button>
		</div>`;
	
	// this module depends on the ajax module
	if(!namespace.ajax){
		throw new Error("this module requires the ajax module");
	}

	initialize();

	///////////////////////////////
	// METHODS
	///////////////////////////////

	function initialize(){
		listContainer.innerHTML = template;
	}

	function errorCallback(statusCode, statusText){
		alert(statusCode + "\n" + statusText);
	}

	document.getElementById("btnGetAll").addEventListener("click", ()=>{
		
		namespace.ajax.send({
			callback: function(responseText){
				var restaurants = JSON.parse(responseText);
				var list = new teekaywebdev.ResponseModule({
					container: detailsContainer,
					objects: restaurants,
					UIheader : `List of all restaurants:`
				});
			},
			url: baseURL,
			method: "GET",
			headers: {"Content-Type":"text/html", "Accept":"application/json"},
			errorCallback: errorCallback
		});
	});

	document.getElementById("btnGetById").addEventListener("click", ()=>{

		var id = prompt("Enter a restaurant id to fetch");

		url = baseURL + id;

		namespace.ajax.send({
			callback: function(responseText){
				var restaurant = JSON.parse(responseText);
				var list = new teekaywebdev.ResponseModule({
					container: detailsContainer,
					objects: restaurant,
					UIheader : `Restaurant data for the id of ${id}:`
				});
			},
			url: url,
			method: "GET",
			headers: {"Content-Type":"text/html", "Accept":"application/json"},
			errorCallback: errorCallback
		});
	});

	document.getElementById("btnPost").addEventListener("click", ()=>{

		var json = userFormModule.submit();

		//if user input was valid, send request
		if(json != null){
			namespace.ajax.send({
				callback: function(responseText){
					var restaurant = JSON.parse(responseText);
					var list = new teekaywebdev.ResponseModule({
						container: detailsContainer,
						objects: restaurant,
						UIheader : `A restaurant was added to the database with the new id of ${restaurant.restaurantId}: `
					});
				},
				url: baseURL,
				method: "POST",
				headers: {"Content-Type":"text/html", "Accept":"application/json"},
				requestBody: json,
				errorCallback: errorCallback
			});
		}
		
	});

	document.getElementById("btnPut").addEventListener("click", ()=>{
		
		var id = prompt("Enter the id of the Restaurant you'd like to update");

		url = baseURL + id;

		var json = userFormModule.submit(id);

		//if user input was valid, send request
		if(json != null){

			namespace.ajax.send({
				callback: function(responseText){
					var restaurant = JSON.parse(responseText);
					var list = new teekaywebdev.ResponseModule({
						container: detailsContainer,
						objects: restaurant,
						UIheader : `A restaurant with the id of ${id} was updated to: `
					});
				},
				url: url,
				method: "PUT",
				headers: {"Content-Type":"text/html", "Accept":"application/json"},
				requestBody: json,
				errorCallback: errorCallback
			});
		}
		
	});

	document.getElementById("btnDelete").addEventListener("click", ()=>{
		
		var id = prompt("Enter a Restaurant id to delete");

		var url = baseURL + id;

		namespace.ajax.send({
			callback: function(responseText){
				var restaurant = JSON.parse(responseText);
				var list = new teekaywebdev.ResponseModule({
					container: detailsContainer,
					objects: restaurant, 
					UIheader : `A restaurant with the id of ${id} was deleted from the database: `
				});
			},
			url: url,
			method: "DELETE",
			headers: {"Content-Type":"text/html", "Accept":"application/json"},
			errorCallback: errorCallback
		});
	});

}