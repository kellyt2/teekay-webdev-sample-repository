var teekaywebdev = teekaywebdev || {};

/**
* Creates an instance of the restaurant list and renders it on the page.
* @param {object} 		options	
* @param {HTML Element} options.container 	The HTML element which will contain the UI
* @param {object}       options.objects     An array of JSON objects (or one object) to display in UI list 
* @param {string}       options.UIheader    An optional string describing result of AJAX call to use as header for container
*/

teekaywebdev.ResponseModule = function(options){

    var container = options.container || null;
    var UIheader = options.UIheader || null;

    initializeList(options.objects);

    function initializeList(objects){
        container.innerHTML = "";
        if(UIheader){
            container.innerHTML = `<h4 class="ml-3">${UIheader}</h4>`;
        }
        if(Array.isArray(objects)){
            //passing array of objects
            objects.forEach(function(object){
                //make a dictionary style list for each object with property names and values
                var template =
                `<dl class="row ml-lg-0 ml-3">`;
                for (property in object) {
                    var key = property;
                    var value = object[property];
                    template += 
                        `<dt class="col-sm-6">${key}</dt>
                        <dd class="col-sm-6">${value}</dd>`;
                 }
                 template +=  `</dl>`;
                container.innerHTML += template;
            });
        }else{
            //only one object being passed (still need plural variable)
            var template =
            `<dl class="row ml-3">`;
            for (property in objects) {
                var key = property;
                var value = objects[property];
                template += 
                    `<dt class="col-sm-6">${key}</dt>
                    <dd class="col-sm-6">${value}</dd>`;
             }
             template +=  `</dl>`;
            container.innerHTML += template;
        } 
    }
};