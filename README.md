# README #

### What is this repository for? ###

* This is a public sample code repository for web and software developer Tara Kelly.
* Includes Android app (Java), Java sample code, RESTful web service (JS), Contact Manager app (JS), and US Race Events app (ASP.NET Core MVC)
* Please see personal website, www.teekaywebdev.com, for more info.

### Who do I talk to? ###

* You may contact me through my website.
